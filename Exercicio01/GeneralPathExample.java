import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JFrame;

public class GeneralPathExample  extends JFrame {
	
	public void paint (Graphics g){
	
		Graphics2D g2d = (Graphics2D)g;
		GeneralPath gp = new GeneralPath();
		Rectangle2D.Double rt = new Rectangle2D.Double(30, 120, 40, 60); 
		g2d.draw(rt);
		//Aqui em baixo é um circulo, tá ok?
		Ellipse2D.Double ed = new Ellipse2D.Double(200-10, 200-10, 2*10, 2*10);
		g2d.draw(ed);
		}

	public static void main(String args[]){

		GeneralPathExample f = new GeneralPathExample();
		f.setTitle("Exercicio 01");
		f.setSize(600,600);
		f.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);	
		f.setVisible(true);
	}

}
