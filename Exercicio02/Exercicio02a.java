import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;

public class Exercicio02a extends JFrame {

    public void paint (Graphics g){
        Graphics2D g2d = (Graphics2D)g;
        Rectangle2D.Double rt = new Rectangle2D.Double(100, 100, 50,100);
        g2d.draw(rt);

        Ellipse2D.Double el = new Ellipse2D.Double(75, 125, 2*20, 2*20);
        g2d.draw(el);

        Area a = new Area(el);
        Area b = new Area(rt);
        a.intersect(b);
        g2d.fill(a);
    }

    public static void main(String args[]){
        Exercicio02a tela = new Exercicio02a();
        tela.setTitle("Exercicio 02 - A");
        tela.setSize(500, 500);
        tela.setVisible(true);
    }
}
