import javax.swing.JFrame;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Exercicio02e extends JFrame {

    public void paint(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        Rectangle2D.Double rt = new Rectangle2D.Double(25, 100, 200, 400);
        g2d.draw(rt);

        Ellipse2D.Double el = new Ellipse2D.Double(200, 600-300, 2*100, 2*100);
        g2d.draw(el);

        Area a = new Area(rt);
        Area b = new Area(el);

        b.exclusiveOr(a);
        g2d.fill(b);

    }

    public static void main(String args[]){
        Exercicio02e f = new Exercicio02e();
        f.setTitle("Exercicio-02e");
        f.setSize(600,600);
        f.setVisible(true);
    }

}
