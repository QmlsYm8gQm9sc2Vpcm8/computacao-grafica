//Criacao de um Barco

import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;

public class Exercicio03 extends JFrame {

    public void paint (Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        GeneralPath casco = new GeneralPath();
        GeneralPath mastro= new GeneralPath();
        GeneralPath velaP = new GeneralPath();
        GeneralPath velaG = new GeneralPath();
        GeneralPath bandeira = new GeneralPath();

        //Casco
        casco.moveTo(0+50,325);
        casco.lineTo(500-50,325);
        casco.lineTo(500-125,400);
        casco.lineTo(0+125,400);
        casco.lineTo(0+50,325);
        g2d.draw(casco);

        //Mastro
        mastro.moveTo(245,325);
        mastro.lineTo(245,0+125);
        mastro.curveTo(245, 0+125, 250,0+120, 255,0+125);
        mastro.lineTo(255,325);
        g2d.draw(mastro);

        //VelaG
        velaG.moveTo(260,320);
        velaG.lineTo(500-65,320);
        velaG.lineTo(260,130);
        velaG.lineTo(260,320);
        g2d.draw(velaG);

        //VelaP
        velaP.moveTo(240,320);
        velaP.lineTo(0+120, 320);
        velaP.lineTo(240,0+180);
        velaP.lineTo(240,320);
        g2d.draw(velaP);

        //Bandeira
        bandeira.moveTo(245,0+125);
        bandeira.lineTo(185,140);
        bandeira.lineTo(245,155);
        g2d.draw(bandeira);
    }

    public static void main(String args[]){
        Exercicio03 f = new Exercicio03();
        f.setTitle("Desenho de um Barco");
        f.setSize(500, 500);
        f.setVisible(true);
    }
}
