import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

public class Exercicio04 extends JFrame {

    public void paint(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        GeneralPath piso = new GeneralPath();
        GeneralPath mastro = new GeneralPath();

        piso.moveTo(0,400);
        piso.lineTo(500,400);
        g2d.draw(piso);

        mastro.moveTo(250-50,400);
        mastro.lineTo(250-50,135);
        mastro.curveTo(250-50,135,250-45,125, 250-40,135);
        mastro.lineTo(250-40,400);
        g2d.draw(mastro);

        Rectangle2D.Double bandeira = new Rectangle2D.Double(250-40,145,180,90);
        g2d.draw(bandeira);
    }

    public static void main(String args[]){
        Exercicio04 f = new Exercicio04();
        f.setTitle("Mastro com uma bandeira.");
        f.setSize(500,500);
        f.setVisible(true);
    }

}
