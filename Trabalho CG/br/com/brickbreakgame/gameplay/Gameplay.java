package br.com.brickbreakgame.gameplay;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import br.com.brickbreakgame.map.MapGenerator;

public class Gameplay extends JPanel implements KeyListener, ActionListener {

	private static final long serialVersionUID = 902810367191932974L;
	
	private static final int GET_POINT = 15;
	private static final int INITIAL_BALL_POS_X = 120;
	private static final int INITIAL_BALL_POS_Y = 350;
	private static final int INITIAL_DIR_BALL_X = -1;
	private static final int INITIAL_DIR_BALL_Y = -2;
	private static final int INITIAL_PLAYER_POS_X = 310;
	private static final int INITIAL_POINTS = 0;
	private static final int INIITAL_TOTAL_BRICKS = 21;
	private static final String GAME_OVER = "Game Over";
	private static final String WON_GAME = "YOU WON";
	
	private boolean play = false;
	private int points;
	private int totalBricks;
	private Timer timer;
	private int delay = 8;
	private int playerX;
	
	private int ballPosX;
	private int ballPosY;
	private int ballDirX;
	private int ballDirY;
	
	private Rectangle hitBoxBall;
	private Rectangle hitBoxPlayer;
	
	private MapGenerator map;
	
	public Gameplay() {
		this.initialGameSettings();

		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		
		this.hitBoxBall = this.createHitBox(this.ballPosX, this.ballPosY, 20, 20);
		this.hitBoxPlayer = this.createHitBox(this.playerX, 550, 100, 8);
		
		this.timer = new Timer(this.delay, this);
		this.timer.start();
	}
	
	public void paint(Graphics g) {
		
		g = this.paintBackGround(g);
		g = this.drawMap(g);
		g = this.paintBorders(g);
		g = this.paintPaddle(g);
		g = this.paintBall(g);
		g = this.paintScore(g);
		
		if(this.isEndGame()) {
			g = this.paintEndGame(g, GAME_OVER);
		}
		
		if(this.wonGame()) {
			g = this.paintEndGame(g, WON_GAME);
		}
		
		g.dispose();
	}
	
	private Graphics paintBall(Graphics ball) {
		ball.setColor(Color.WHITE);
		ball.fillOval(ballPosX, ballPosY, 20, 20);
		
		return ball;
	}
	
	private Graphics paintPaddle(Graphics paddle) {
		paddle.setColor(Color.YELLOW);
		paddle.fillRect(playerX, 550, 100, 8);
		
		return paddle;
	}
	
	private Graphics paintBorders(Graphics borders) {
		borders.setColor(Color.YELLOW);
		borders.fillRect(0, 0, 3, 598);
		borders.fillRect(0, 0, 697, 3);
		borders.fillRect(697, 0, 3, 590);

		return borders;
	}
	
	private Graphics paintEndGame(Graphics endGame, String message) {
		endGame.setColor(Color.RED);
		endGame.setFont(new Font("serif", Font.BOLD, 30));
		endGame.drawString(message + ", Final Score: "+ this.points, 125, 300);
		
		endGame.setFont(new Font("serif", Font.BOLD, 20));
		endGame.drawString("Press Enter to restart the game!", 175, 350);
		
		return endGame;
	}
	
	private Graphics paintBackGround(Graphics backGround) {
		backGround.setColor(Color.DARK_GRAY);
		backGround.fillRect(1, 1, 698, 598);
		
		return backGround;
	}
	
	private Graphics drawMap(Graphics map) {
		return this.map.draw((Graphics2D) map);
	}
	
	private Graphics paintScore(Graphics score) {
		score.setColor(Color.ORANGE);
		score.setFont(new Font("serif", Font.BOLD, 25));
		
		score.drawString(""+this.points, 590, 30);
		
		return score;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.timer.start();
		
		this.ballMove();
		
		this.repaint();
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			
			if(this.playerX >= 590 ) {
				this.playerX = 590;
			} else {
				this.moveRight();
			}
			
		} else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			
			if(this.playerX <= 10) {
				this.playerX = 10;
			} else {
				this.moveLeft();
			}
			
		} else if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			if(!this.play) {
				this.initialGameSettings();
				this.play = true;
				repaint();
			}
		}
		
	}
	
	private void initialGameSettings() {
		this.play = false;
		this.points = INITIAL_POINTS;
		this.totalBricks = INIITAL_TOTAL_BRICKS;	
		this.ballPosX = INITIAL_BALL_POS_X;
		this.ballPosY = INITIAL_BALL_POS_Y;
		this.ballDirX = INITIAL_DIR_BALL_X;
		this.ballDirY = INITIAL_DIR_BALL_Y;
		this.playerX = INITIAL_PLAYER_POS_X;
		
		this.map = new MapGenerator();
	}	
	
	private void ballMove() {
		if(this.play) {
			this.ballPosX += this.ballDirX;
			this.ballPosY += this.ballDirY;
			
			this.updateHitBox(this.ballPosX, this.ballPosY);
			
			this.isBallTouchPlayer();
			
			this.isBallTouchBrick();

			if(this.ballPosX < 0) {
				 this.ballDirX = -this.ballDirX;
			}
			
			if(this.ballPosY < 0) {
				this.ballDirY = -this.ballDirY;
			}
			
			if(this.ballPosX > 670) {
				this.ballDirX = -this.ballDirX;
			}
		}
	}
	
	private void isBallTouchPlayer() {
		if(this.hitBoxBall.intersects(this.hitBoxPlayer)) {
			this.ballDirY = -this.ballDirY;
		}
	}
	
	private void isBallTouchBrick() {
		touch:	for(int row = 0; row < this.map.map.length; row++) {
					for(int col = 0; col < this.map.map[0].length; col++) {
						if(map.map[row][col] > 0) {
							int brickX = col * map.getBrickWidth() + 80;
							int brickY = row * map.getBrickHeigth() + 50;
							
							Rectangle hitBoxBrick = this.createHitBox(brickX, brickY, map.getBrickWidth(), map.getBrickHeigth());
							
							if(this.hitBoxBall.intersects(hitBoxBrick)) {
								this.map.setBrickValue(0, row, col);
								this.breakOneBrick();
								this.scoreUp();
								
								
								if(this.ballPosX + 19 <= hitBoxBrick.x || this.ballPosX + 1 >= hitBoxBrick.x + hitBoxBrick.width) {
									this.ballDirX = -this.ballDirX;
								} else {
									this.ballDirY = -this.ballDirY;
								}
								
								break touch;
							}
						}
					}
				}
	}
	
	private void moveRight() {
		this.play = true;
		this.playerX += 20;
		this.updateHitBox(playerX, 0);
	}
	
	private void moveLeft() {
		this.play = true;
		this.playerX -= 20;
		this.updateHitBox(playerX, 0);
	}
	
	private Rectangle createHitBox(int posX, int posY, int width, int heigth) {
		Rectangle hitBox = new Rectangle(posX, posY, width, heigth);
		
		return hitBox;
	}
	
	private void updateHitBox(int posX, int posY) {
		if(posY == 0) {
			this.hitBoxPlayer.x = posX;
		} else {
			this.hitBoxBall.x = posX;
			this.hitBoxBall.y = posY;
		}
	}
	
	private boolean wonGame() {
		if(this.totalBricks == 0) {
			this.play = false;
			return true;
		}
		
		return false;
	}
	
	private boolean isEndGame() {
		if(this.ballPosY > 570) {
			this.play = false;
			this.ballDirX = 0;
			this.ballDirY = 0;
			
			return true;
		} else {
			return false;
		}
	}
	
	private void breakOneBrick() {
		this.totalBricks--;
	}
	
	private void scoreUp() {
		this.points += GET_POINT;
	}

	@Override
	public void keyReleased(KeyEvent e) {	
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
	
	

}
