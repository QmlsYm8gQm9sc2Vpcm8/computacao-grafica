package br.com.brickbreakgame.main;

import javax.swing.JFrame;

import br.com.brickbreakgame.gameplay.Gameplay;

public class Main {
	
	private static final String TITLE = "Brick Break Game version [0.1]";
	private static final int WIDTH = 700;
	private static final int HEIGHT = 600;
	private static final int TOP = 300;
	private static final int LEFT = 90;
	
	public static void main(String[] args) {
		JFrame jf = new JFrame();
		
		Gameplay gp = new Gameplay();
		
		jf.setBounds(TOP, LEFT, WIDTH, HEIGHT);
		jf.setTitle(TITLE);
		jf.setResizable(false);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.add(gp);
	}
}
