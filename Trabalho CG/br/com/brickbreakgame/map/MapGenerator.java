package br.com.brickbreakgame.map;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MapGenerator {
	public int map[][];
	private int brickWidth;
	private int brickHeight;
	
	private int rowBricks = 3;
	private int colBricks = 7;
	
	public MapGenerator() {
		this.map = new int[this.rowBricks][this.colBricks];
		
		this.brickWidth = 540 / this.colBricks;
		this.brickHeight = 150 / this.rowBricks;
		
		this.buildMap(true, null);
		

	}
	
	private Graphics buildMap(boolean init, Graphics2D brick) {
		
		for(int countRow = 0; countRow < this.map.length; countRow++) {
			for(int countCol = 0; countCol < this.map[0].length; countCol++) {
				
				if(init) {
					this.map[countRow][countCol] = 1;
				} else {
					if(map[countRow][countCol] > 0) {
						brick.setColor(Color.YELLOW);
						brick.fillRect(
								(countCol * this.brickWidth + 80), 
								(countRow * this.brickHeight + 50), 
								this.brickWidth, 
								this.brickHeight);
						
						brick.setStroke(new BasicStroke(3));
						brick.setColor(Color.YELLOW);
						brick.drawRect(
								(countCol * this.brickWidth + 80), 
								(countRow * this.brickHeight + 50), 
								this.brickWidth, 
								this.brickHeight);
						
					}
				}
				
			}
		}
		
		return (Graphics) brick;
		
	}
	
	public void setBrickValue(int value, int row, int col) {
		this.map[row][col] = value;
	}
	
	public Graphics draw(Graphics2D object) {
		return this.buildMap(false, object);
	}
	
	public int getBrickWidth() {
		return this.brickWidth;
	}
	
	public int getBrickHeigth() {
		return this.brickHeight;
	}
}
